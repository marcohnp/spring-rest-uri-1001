package com.uri2;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@Api(value="API Rest Sum")
@RestController
public class SumController {
	
	@GetMapping("/sum")
	public Sum sum(@RequestParam int a, @RequestParam int b) {
		Sum s = new Sum(a,b);
		return s;
	}

}
