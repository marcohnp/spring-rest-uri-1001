package com.uri2;

public class Sum {
	
	private int a;
	private int b;
	private int sum;
	
	public Sum(int a, int b) {
		this.a = a;
		this.b = b;
		this.sum = a + b;
	}

	public int getA() {
		return a;
	}

	public int getB() {
		return b;
	}

	public int getSum() {
		return sum;
	}

}
