package com.uri2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestUri1002Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestUri1002Application.class, args);
	}

}
