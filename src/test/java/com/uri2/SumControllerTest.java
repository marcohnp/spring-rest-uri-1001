package com.uri2;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@AutoConfigureMockMvc
public class SumControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testSumSuccess() throws Exception {
				mockMvc.perform(MockMvcRequestBuilders.get("/sum")
				.param("a", "5")
				.param("b", "5")
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json"))
				.andExpect(jsonPath("$.a").value(5))
				.andExpect(jsonPath("$.b").value(5))
	            .andExpect(jsonPath("$.sum").value(10));
	}
}
