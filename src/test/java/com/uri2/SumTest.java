package com.uri2;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SumTest {

	@Test
	void testGetSum() {
		Sum s = new Sum(10,9);
		int expected = 19;
		int actual = s.getSum();
		assertTrue(expected==actual);
	}

}
